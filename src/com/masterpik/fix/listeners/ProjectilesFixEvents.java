package com.masterpik.fix.listeners;

import com.masterpik.fix.ProjectilesFix;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.ArrayList;

public class ProjectilesFixEvents implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void PlayerJoinEvent(PlayerJoinEvent event) {
        ProjectilesFix.projectiles.put(event.getPlayer(), new ArrayList<Projectile>());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        ProjectilesFix.projectiles.remove(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void PlayerDeathEvent(PlayerDeathEvent event) {
        ProjectilesFix.DeleteProjectilesFromPlayer(event.getEntity());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void PlayerTeleportEvent(PlayerTeleportEvent event) {
        ProjectilesFix.DeleteProjectilesFromPlayer(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void ProjectileLaunchEvent(ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            ProjectilesFix.projectiles.get(event.getEntity().getShooter()).add(event.getEntity());
        }
    }

}
