package com.masterpik.fix;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;

import java.util.ArrayList;
import java.util.HashMap;

public class ProjectilesFix {

    public static HashMap<Player, ArrayList<Projectile>> projectiles;

    public static void ProjectilesFixInit() {

        projectiles = new HashMap<>();

    }

    public static void DeleteProjectilesFromPlayer(Player player) {

        ArrayList<Projectile> playerProjectile = ProjectilesFix.projectiles.get(player);
        if (playerProjectile != null) {
            int bucle1 = 0;

            while (bucle1 < playerProjectile.size()) {

                playerProjectile.get(bucle1).remove();

                bucle1++;
            }
        }

    }


}
