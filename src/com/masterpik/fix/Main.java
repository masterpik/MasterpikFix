package com.masterpik.fix;

import com.masterpik.fix.listeners.ColissionsEvents;
import com.masterpik.fix.listeners.ProjectilesFixEvents;
import com.masterpik.fix.pushing.StopPushing;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{

    public static Plugin plugin;


    @Override
    public void onEnable() {

        plugin = this;

        //StopPushing.onEnable();

        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new ProjectilesFixEvents(), this);
        //pluginManager.registerEvents(new ColissionsEvents(), this);

        ProjectilesFix.ProjectilesFixInit();



    }

}
