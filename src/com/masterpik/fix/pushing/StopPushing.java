package com.masterpik.fix.pushing;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

import com.masterpik.fix.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Team.Option;
import org.bukkit.scoreboard.Team.OptionStatus;

public class StopPushing implements Listener
{
    private static StopPushing main;
    private static String[] all = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
    private static Random r = new Random();

    private static Class<?> ct = NMSUtils.getOBCClass("scoreboard.CraftTeam");
    private static Field t = NMSUtils.getField(ct, "team");

    private static Class<?> etp = NMSUtils.getNMSClassSilent("EnumTeamPush", "ScoreboardTeamBase");
    private static Class<?> st = NMSUtils.getNMSClass("ScoreboardTeam");
    private static Method a = NMSUtils.getMethod(st, "a", new Class[] { etp });
    private static Method k = NMSUtils.getMethod(st, "k", new Class[0]);
    private static Object ALLOW;
    private static Object NEVER;
    private static boolean old = false;

    static {
        try {
            Class test = NMSUtils.getInnerClassSilent(Team.class, "Option");
            if (test == null) {
                old = true;
                ct = NMSUtils.getOBCClass("scoreboard.CraftTeam");
                t = NMSUtils.getField(ct, "team");
                etp = NMSUtils.getNMSClassSilent("EnumTeamPush", "ScoreboardTeamBase");
                st = NMSUtils.getNMSClass("ScoreboardTeam");
                a = NMSUtils.getMethod(st, "a", new Class[] { etp });
                ALLOW = etp.getEnumConstants()[0];
                NEVER = etp.getEnumConstants()[1];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void onEnable()
    {

        Main.plugin.getServer().getScheduler().scheduleAsyncRepeatingTask(Main.plugin, new Runnable() {
            public void run() {
                Player player;
                for (Iterator localIterator = Bukkit.getOnlinePlayers().iterator(); localIterator.hasNext(); set(player, false))
                    player = (Player) localIterator.next();
            }
        }
                , 200L, 200L);
    }

    @EventHandler(priority=EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        set(event.getPlayer(), true);
    }

    public static void set(Team team, Player player)
    {
        set(team, player, false);
    }

    public static void set(Team team, Player player, boolean force)
    {
        if (!team.getOption(Team.Option.COLLISION_RULE).equals(Team.OptionStatus.NEVER)
                || force) {
            team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
            //Bukkit.getLogger().info("collision rule update for player "+player.getName());
        }
    }

    public static void setOld(Team team, Player player)
    {
        try
        {
            Object o = t.get(team);
            Object o1 = k.invoke(o, new Object[0]);
            if (false/*player.hasPermission("stoppushing.allow")*/) {
                if (!o1.equals(ALLOW)) a.invoke(o, new Object[] { ALLOW });
            }
            else if (!o1.equals(NEVER)) a.invoke(o, new Object[] { NEVER });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void set(Player player, boolean force) {
        Scoreboard sb = player.getScoreboard();
        if (sb == null) {
            sb = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        String name = player.getName();
        Team team = sb.getEntryTeam(name);
        if (team == null) {
            String n = name;
            while (true) {
                if (sb.getTeam(n) == null) {
                    team = sb.registerNewTeam(n);
                    team.addEntry(name);
                    break;
                }
                n = n + all[r.nextInt(all.length - 1)];
                if (n.length() > 16) n = n.substring(1);
            }
        }
        if (!old)
            set(team, player, force);
        else
            setOld(team, player);
    }

    public static void set(Player player) {
        set(player, false);
    }
}
